/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {}
  },
  plugins: [require('daisyui')],
  daisyui: {
    themes: [
      {
        mytheme: {
          primary: '#ff8800',
          secondary: '#6D3A9C',
          accent: '#22d3ee',
          neutral: '#1B1D1D',
          'base-100': '#212121',
          info: '#2563EB',
          success: '#16A34A',
          warning: '#fbbf24',
          error: '#DC2626'
        }
      }
    ]
  }
};
