import { ProgrammingLanguage } from '@lujobi-projects/ebnf-to-regex';
import type { PageServerLoad } from './$types';

export const load = (async () => {
  return {
    programming_languages: Object.keys(ProgrammingLanguage).filter((v) => isNaN(Number(v)))
  };
}) satisfies PageServerLoad;
