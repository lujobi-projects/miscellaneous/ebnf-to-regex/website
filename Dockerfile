# stage build
FROM node:20-alpine as builder
WORKDIR /app
# copy everything to the container
COPY . .
# clean install all dependencies
RUN yarn install --frozen-lockfile
# remove potential security issues
RUN yarn npm audit
# build SvelteKit app
RUN yarn run build

# stage run
FROM node:20-alpine
WORKDIR /app
# copy dependency list
COPY --from=builder /app/package*.json ./
COPY .yarnrc ./
# clean install dependencies, no devDependencies, no prepare script
RUN yarn install --frozen-lockfile --ignore-scripts --production
# remove potential security issues
RUN yarn npm audit
# copy built SvelteKit app to /app
COPY --from=builder /app/build ./

EXPOSE 3000
CMD ["node", "./index.js"]
